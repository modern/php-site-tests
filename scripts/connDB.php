<?php
header("Content-Type: text/html; charset=utf-8");

$dbname = 'booktest';

$db = new mysqli("localhost", "root", "root", $dbname)
or handle_error("возникла проблема, связанная с подключением к базе данных, " .
    "содержащей нужную информацию.",
    $db->error);
echo "<p>Вы подключились к MySQL!</p>";

$sql = "SHOW TABLES";

$result = $db->query($sql);

while($table = mysqli_fetch_array($result)) { // go through each row that was returned in $result
    echo($table[0] . "<BR>");    // print the table that was returned on that row.
}

$db->close();