<?php
header("Content-Type: text/html; charset=utf-8");
require 'database_connection.php';

$query_text = $_REQUEST['query'];
$result = $db->query($query_text);

$return_rows = true;
if (preg_match("^ */(CREATE|INSERT|UPDATE|DELETE|DROP)/i", $query_text)) {
    $return_rows = false;
}

if (!$result) {
    die("<p>Ошибка при выполнении SQL-запроса " . $query_text . ": " .
        $db->error . "</p>");
}
echo "<p>Результаты вашего запроса:</p>";
echo "<ul>";
while ($row = @mysqli_fetch_row($result)) {
    echo "<li>{$row[0]}</li>";
}
echo "</ul>";

// + любое не нулевое кол-во
// * отсутствие или любое кол-во