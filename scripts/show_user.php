<?php
header("Content-Type: text/html; charset=utf-8");
require_once 'database_connection.php';
require_once 'view.php';
//require_once 'authorize.php';


$user_id = $_REQUEST['user_id'];
// Создание инструкции SELECT
$select_query = "SELECT * FROM users WHERE user_id = " . $user_id;
// Запуск запроса
$result = $db->query($select_query);

if ($result) {
    $row = mysqli_fetch_array($result);
    $first_name = $row['first_name'];
    $last_name = $row['last_name'];
    $bio = preg_replace("/[\r\n]+/", "</p><p>", $row['bio']);
    $email = $row['email'];
    $facebook_url = $row['facebook_url'];
    $twitter_handle = $row['twitter_handle'];
    $user_image = $row['user_pic_path'];
    $twitter_url = "http://www.twitter.com/";
    $position = strpos($twitter_handle, "@");
    if ($position === false) {
        $twitter_url = $twitter_url . $twitter_handle;
    } else {
        $twitter_url = $twitter_url . substr($twitter_handle, $position + 1);
    }
    $twitter_url = "http://www.twitter.com/" . substr($twitter_handle, $position + 1);
} else {
    handle_error("возникла проблема с поиском вашей " . "информации на нашей системе.", "Ошибка обнаружения пользователя с ID {$user_id}");
}

page_start("Профиль");
?>

<div id="content">
    <div class="user_profile">
        <h1><?php echo "$first_name $last_name"; ?></h1>

        <p><img src="<?php echo $user_image; ?>" class="user_pic" />
            <?php echo $bio; ?></p>

        <p class="contact_info">Поддерживайте связь с <php $first_name; ?>:</p>
        <ul>
            <li>...
                <a href="<?php echo $email; ?>">по электронной почте</a></li>
            <li>...
                <a href="<?php echo $facebook_url; ?>">путем посещения его страницы
                    <li>... путем<a href="<?php echo $twitter_url; ?>">отслеживания его сообщений в Twitter</a></li>
        </ul>
    </div>
</div>
<div id="footer"></div>
</body>
</html>