<?php

if (!isset($_SERVER['PHP_AUTH_USER']) ||
    !isset($_SERVER['PHP_AUTH_PW'])) {
    header('HTTP/1.1 401 Unauthorized');
    header('WWW-Authenticate: Basic realm="The Social Site"');
    exit("Здесь нужно указать верное имя пользователя и пароль." ."Проходите дальше. Здесь вам нечего смотреть.");
}

require_once 'database_connection.php';

// Поиск предоставленных пользователем полномочий
$query = sprintf("SELECT user_id, username FROM users WHERE username= '%s' AND password = '%s';",
    trim($_SERVER['PHP_AUTH_USER']),
    trim($_SERVER['PHP_AUTH_PW']));
$results = $db->query($query);

if (mysqli_num_rows($results) == 1) {
    $result = mysqli_fetch_array($results);
    $current_user_id = $result['user_id'];
    $current_username = $result['username'];
} else {
    header('HTTP/1.1 401 Unauthorized');
    header('WWW-Authenticate: Basic realm="The Social Site"');
    exit("Здесь нужно указать верное имя пользователя и пароль." .
        "Проходите дальше. Здесь вам нечего смотреть.");
}