<?php

header("Content-Type: text/html; charset=utf-8");
//require_once 'authorize.php';
require_once 'database_connection.php';
require_once 'view.php';

// Создание инструкции SELECT
$select_users = "SELECT user_id, first_name, last_name, email  FROM users";
// Запуск запроса
$result = $db->query($select_users);

// Проверка наличия сообщения, предназначенного для отображения
if (isset($_REQUEST['success_message'])) {
    $msg = $_REQUEST['success_message'];
}

page_start("Пользователи");
?>

<div id="content">
    <ul>
        <?php
        while ($user = mysqli_fetch_array($result)) {
            $user_row = sprintf(
                "<li><a href='show_user.php?user_id=%d'>%s %s</a> " .
                "(<a href='mailto:%s'>%s</a>) " .
                "<a href='javascript:delete_user(%d);'><img " .
                "class='delete_user' src='../images/delete.png' " .
                "width='15' /></a></li>",
                $user['user_id'], $user['first_name'], $user['last_name'],
                $user['email'], $user['email'], $user['user_id']);
            echo $user_row;
        }
        ?>
    </ul>
</div>
<div id="footer"></div>
</body>
</html>