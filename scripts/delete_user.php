<?php

header("Content-Type: text/html; charset=utf-8");
require_once 'authorize.php';
require_once 'database_connection.php';

// Получение идентификатора удаляемого пользователя
$user_id= $_REQUEST['user_id'];
// Создание инструкцииDELETE
$delete_query = sprintf("DELETE FROM users WHERE user_id = %d", $user_id);
// Удаление пользователя из базы данных
$db->query($delete_query);

$msg = "Указанный пользователь был удален.";
header("Location: show_users.php?success_message={$msg}");
exit();