<?php

// Установка режима отладки
define("DEBUG_MODE", false);

define("DATABASE_HOST", "localhost");
define("DATABASE_USERNAME", "root");
define("DATABASE_PASSWORD", "root");
define("DATABASE_NAME", "booktest");

$debug_mode = true;

// Выдача отчетов об ошибках
if($debug_mode) {
    error_reporting(E_ALL);
} else {
// Выключение выдачи отчетов об ошибках
    error_reporting(0);
}
function debug_print($message) {
    if (DEBUG_MODE) {
        echo $message;
    }
}

function handle_error($user_error_message, $system_error_message) {
    header("Location: show_error.php?" .
        "error_message={$user_error_message}&" .
        "system_error_message={$system_error_message}");
}

?>