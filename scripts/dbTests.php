<?php

define("DATABASE_HOST", "localhost");
define("DATABASE_USERNAME", "root");
define("DATABASE_PASSWORD", "root");
define("DATABASE_NAME", "booktest");

$db = new mysqli(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_NAME);

$select_query = "SELECT * FROM images WHERE image_id = 2";

$result = $db->query($select_query);

$image = mysqli_fetch_assoc($result);

echo $image['filename'];

$result->close();