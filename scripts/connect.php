<?php
header("Content-Type: text/html; charset=utf-8");
require_once 'app_config.php';

$db = new mysqli(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_NAME)
    or handle_error("возникла проблема, связанная с подключением к базе данных, " . "содержащей нужную информацию.",
    $db->error);

$sql = "SHOW TABLES;";

$result = $db->query($sql);

if (!$result) {
    die("<p>Ошибка при выводе перечня таблиц: " . $db->error . "</p>");
}

echo "<p>Таблицы, имеющиеся в базе данных:</p>";
echo "<ul>";

while ($row = mysqli_fetch_row($result)) {
    //echo "<li>Таблица: " . $row[0] . "</li>";
    echo "<li>Таблица: {$row[0]}</li>";
}
echo "</ul>";