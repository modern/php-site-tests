<?php
header("Content-Type: text/html; charset=utf-8");
//require_once 'app_config.php';
require_once 'database_connection.php';

$upload_dir = "../uploads/profile_pics/";
$image_fieldname = "user_pic";

$php_errors = array(1 => 'Превышен макс. размер файла, указанный в php.ini',
    2 => 'Превышен макс. размер файла, указанный в форме HTML',
    3 => 'Была отправлена только часть файла',
    4 => 'Файл для отправки не был выбран.');

$first_name = trim($_REQUEST['first_name']);
$last_name = trim($_REQUEST['last_name']);
$username = trim($_REQUEST['username']);
$password = trim($_REQUEST['password']);
$email = trim($_REQUEST['email']);
$bio = trim($_REQUEST['bio']);
$facebook_url = str_replace("facebook.org", "facebook.com", trim($_REQUEST['facebook_url']));
$position = strpos($facebook_url, "facebook.com");
if ($position === false) {
    $facebook_url = "http://www.facebook.com/" . $facebook_url;
}

$twitter_handle = trim($_REQUEST['twitter_handle']);
$twitter_url = "http://www.twitter.com/";
$position = strpos($twitter_handle, "@");

if ($position === false) {
    $twitter_url = $twitter_url . $twitter_handle;
} else {
    $twitter_url = $twitter_url .
        substr($twitter_handle, $position + 1);
}

// Проверка отсутствия ошибки при отправке изображения
($_FILES[$image_fieldname]['error'] == 0) or handle_error("сервер не может получить выбранное вами изображение.",
    $php_errors[$_FILES[$image_fieldname]['error']]);

// Является ли этот файл результатом нормальной отправки?
@is_uploaded_file($_FILES[$image_fieldname]['tmp_name']) or handle_error("вы попытались совершить безнравственный поступок. Позор!",
        "Запрос на отправку: файл назывался " . "'{$_FILES[$image_fieldname]['tmp_name']}'");

// Действительно ли это изображение?
@getimagesize($_FILES[$image_fieldname]['tmp_name']) or handle_error("вы выбрали файл для своего фото, " .
        "который не является изображением.", "{$_FILES[$image_fieldname]['tmp_name']} " .
        "не является настоящим файлом изображения.");

// Присваивание файлу уникального имени
$now = time();
while (file_exists($upload_filename = $upload_dir . $now .
    '-' .
    $_FILES[$image_fieldname]['name'])) {
    $now++;
}

// И наконец, перемещение файла на его постоянное место
move_uploaded_file($_FILES[$image_fieldname]['tmp_name'], $upload_filename)
    or handle_error("возникла проблема сохранения вашего изображения " .
        "в его постоянном месте.",
        "ошибка, связанная с правами доступа при перемещении " .
        "файла в {$upload_filename}");

$insert_sql = "INSERT INTO users (first_name, last_name, username, password, email, bio, facebook_url, twitter_handle, user_pic_path)
                VALUES ('{$first_name}', '{$last_name}', '{$username}','{$password}', '{$email}', '{$bio}', '{$facebook_url}', '{$twitter_handle}', '{$upload_filename}' );";

$db->query($insert_sql) or die("<p>Ошибка при выполнении SQL-запроса " . $insert_sql . ": " . $db->error . "</p>");

header("Location: show_user.php?user_id=" . mysqli_insert_id($db));
exit();
?>