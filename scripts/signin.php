<?php
header("Content-Type: text/html; charset=utf-8");
require_once 'database_connection.php';
require_once 'view.php';

$error_message = "";

// Если пользователь зарегистрировался, будет установлен cookie-файл user_id
if (!isset($_COOKIE['user_id'])) {
// Проверка отправки формы регистрации с username для входа в приложение
    if (isset($_POST['username'])) {
// Попытка зарегистрировать пользователя
        $username = trim($_REQUEST['username']);
        $password = trim($_REQUEST['password']);

        // Поиск пользователя
        $query = sprintf("SELECT user_id, username FROM users " . " WHERE username = '%s' AND " .
            " password = '%s';", $username, $password);

        $results = $db->query($query);

        if (mysqli_num_rows($results) == 1) {
            $result = mysqli_fetch_array($results);
            $user_id = $result['user_id'];
            setcookie('user_id', $user_id);
            setcookie('username', $result['username']);
            header("Location: show_user.php");
        } else{
            // Если пользователь не найден, выдача ошибки
            $error_message = "Вы дали неверную комбинацию имя пользователя-пароль.";
        }
    }

    page_start("Регистрация", NULL, NULL, $error_message);

    ?>

    <html>
    <div id="content">
        <h1>Регистрация в клубе</h1>

        <form id="signin_form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
            <fieldset>
                <label for="username">Имя пользователя:</label>
                <input type="text" name="username" id="username" size="20" value="<?php if (isset($username)) echo $username; ?>"/>
                <br/>
                <label for="password">Пароль:</label>
                <input type="password" name="password" id="password" size="20"/>
            </fieldset>
            <br/>
            <fieldset class="center">
                <input type="submit" value="Зарегистрироваться"/>
            </fieldset>
        </form>
    </div>
    <div id="footer"></div>
    </body>
    </html>

<?php
} else {
    header("Location: show_user.php");
}
?>